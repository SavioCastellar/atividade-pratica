# Front-End Mobile

# Sprint 4 - Questões práticas

**Questão 3** <br /><br />
```process.argv``` é utilizado para capturar inputs da linha de comando e o ```slice(2)``` remove os dois primeiros elementos da lista, o node e o path.<br />

```ruby
const args = process.argv.slice(2);
```

Já o código abaixo printa os argumentos passados como parâmetro para a calculadora -> ```node calculadora.js arg1, arg2```. 
```ruby
console.log(args);

```

**Questão 5** <br /><br />
```parseInt()``` faz a conversão do argumento passado para inteiro. Caso não seja possível realizar essa operação, a execução retorna ```NaN```
```ruby
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const args = process.argv.slice(2);

soma();
```

**Questão 7** <br /><br />
Correção do código:<br />
```ruby
const soma = () => {
    console.log(parseInt(args[1]) + parseInt(args[2]));
};

const sub = () => {
    console.log(parseInt(args[1]) - parseInt(args[2]));
};

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', args[0]);
};
```
**Questão 7** <br /><br />
Função ```eval()```: Permite a leitura de strings para utilizá-las nas operações matemáticas.

**Questão 10** <br /><br />
Para rodar o novo código, é necessário digitar ```...\atividade-pratica> node calculadora.js "arg1" "operator" "arg2"``` no terminal.
Exemplo:
```ruby
...\atividade-pratica> node calculadora.js "2" "*" "5"
10
```
O novo código é capaz de realizar as seguinte operações:
```adição (+)```
```subtração (-)```
```multiplicação (*)```
```divisão (/)```